package nd.practice;

/**
 * Application to run Calculator from CLI.
 * 
 * @author Nathan Duckett
 *
 */
public class Application {

	/**
	 * Main function taking arguments to parse and calculate the results.
	 * 
	 * @param args Program arguments which contain {mode} [numbers]+
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("Invalid arguments passed");
			return;
		}

		int[] numbers = parseNumbers(args);

		runMode(args[0], numbers);
	}

	/**
	 * Parse all of the numbers provided in the arguments for calculation.
	 * 
	 * @param args Arguments received into the program.
	 * @return Int array containing the parsed values from the arguments.
	 */
	private static int[] parseNumbers(String[] args) {
		int offset = 1;
		int[] numbers = new int[args.length - offset];
		for (int i = offset; i < args.length; i++) {
			numbers[i - offset] = Integer.parseInt(args[i]);
		}

		return numbers;
	}

	/**
	 * Parse the mode and apply the correct operation to the numbers.
	 * 
	 * @param mode    String value of the mode which should be run.
	 * @param numbers Extracted numbers to be applied in the operation.
	 */
	private static void runMode(String mode, int[] numbers) {
		switch (mode) {
		case "add":
			print(Calculator.add(numbers));
			break;
		case "subtract":
			print(Calculator.subtract(numbers));
			break;
		case "multiply":
			print(Calculator.multiply(numbers));
			break;
		case "divide":
			print(Calculator.divide(numbers));
			break;
		default:
			System.out.println("No valid mode provided");
			return;
		}
	}

	/**
	 * Helper method to output the value to the expected place.
	 * 
	 * @param result Result from the calculation to be printed.
	 */
	private static void print(int result) {
		System.out.println(result);
	}
}
