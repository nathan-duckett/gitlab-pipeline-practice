package nd.practice;

/**
 * Basic calculator class with functionality.
 * 
 * @author Nathan Duckett
 *
 */
public class Calculator {

	/**
	 * Add two or more numbers together.
	 * 
	 * @param nums Array of numbers to add together.
	 * @return Sum of the numbers added up.
	 */
	public static int add(int... nums) {
		int result = 0;
		for (int number : nums) {
			result += number;
		}

		return result;
	}

	/**
	 * Subtract one or numbers from the first number.
	 * 
	 * @param nums Array of numbers to subtract from the first.
	 * @return Result of the numbers subtracted from the first value.
	 */
	public static int subtract(int... nums) {
		int result = nums[0];
		for (int i = 1; i < nums.length; i++) {
			result -= nums[i];
		}

		return result;
	}

	/**
	 * Multiply all of the numbers together.
	 * 
	 * @param nums Array of numbers to multiply together.
	 * @return Result of the product of all numbers multiplied together.
	 */
	public static int multiply(int... nums) {
		int result = nums[0];
		for (int i = 1; i < nums.length; i++) {
			result *= nums[i];
		}

		return result;
	}

	/**
	 * Divide one or more numbers from the first number.
	 * 
	 * @param nums Array of numbers to divide from the first.
	 * @return Result of the numbers divided by the first value.
	 */
	public static int divide(int... nums) {
		int result = nums[0];
		for (int i = 1; i < nums.length; i++) {
			result /= nums[i];
		}

		return result;
	}
}
