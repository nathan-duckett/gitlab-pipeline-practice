package test.nd.practice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nd.practice.Calculator;

//import net.ddns.ndap.Calculator;

/**
 * Basic unit tests to run on the Calculator class.
 * @author Nathan Duckett
 *
 */
public class CalculatorTest {

	@Test
	void test_add_01() {
		assertEquals(4, Calculator.add(2, 2));
	}
	
	@Test
	void test_subtract_01() {
		assertEquals(2, Calculator.subtract(4, 2));
	}
	
	@Test
	void test_multiply_01() {
		assertEquals(4, Calculator.multiply(2, 2));
	}
	
	@Test
	void test_divide_01() {
		assertEquals(2, Calculator.divide(4, 2));
	}
}
