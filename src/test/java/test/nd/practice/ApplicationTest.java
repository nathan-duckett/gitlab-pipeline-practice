package test.nd.practice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nd.practice.Application;

public class ApplicationTest {
    private ByteArrayOutputStream myOut;


    @BeforeEach
    void setup_std_out_capture() {
        this.myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));
    }

    @Test
    void test_add() {
        Application.main(new String[]{"add", "2", "2"});

        assertEquals("4\n", myOut.toString());
    }

    @Test
    void test_sub() {
        Application.main(new String[]{"subtract", "4", "2"});

        assertEquals("2\n", myOut.toString());
    }

    @Test
    void test_multiply() {
        Application.main(new String[]{"multiply", "2", "2"});

        assertEquals("4\n", myOut.toString());
    }

    @Test
    void test_divide() {
        Application.main(new String[]{"divide", "4", "2"});

        assertEquals("2\n", myOut.toString());
    }
}